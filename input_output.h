#ifndef INPUT_OUTPUT_H_
#define INPUT_OUTPUT_H_

const int PLAY_BLACK;
const int PLAY_WHITE;
const int END_PROGRAM;

void ask_for_command(void);
int get_command(void);
void print_board(State state);
int is_input_valid(char a, char b, uint64_t legal_moves);
uint64_t get_input(uint64_t legal_moves);
void play_human(State *state);

#endif