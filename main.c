// AI for a board game Othello
// Autor: William Tatarko
// Documentation in "./dokumentation.pdf"

// Imports
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "definitions.h"
#include "input_output.h"
#include "game_helper.h"
#include "evaluation.h"

// Main function that contains the game loop
int main (void) {
	srand(time(NULL)); // randomizes random function
	
	int command = get_command();
	while(command != END_PROGRAM) {		
		if (command == PLAY_BLACK) play_game(BLACK);
		else if (command == PLAY_WHITE) play_game(WHITE);
		
		printf("To return to the menu press enter\n");
		char ch;
		scanf("%c", &ch);
		command = get_command();
	}

	return 0;
}