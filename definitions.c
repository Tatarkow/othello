// Constants for distinguishing players and different stones on the board
const char EMPTY = 0;
const char BLACK = 1;
const char WHITE = 2;

// Constats to make code a bit prettier
const char FALSE = 0;
const char TRUE = 1;

// Constant for maximal value used as infinity
const int MAX = 2147483647;