#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "definitions.h"
#include "input_output.h"
#include "game_helper.h"
#include "evaluation.h"

void play_random(State *state);
void play_negamax_depth_limited(State *state, int evaluation[3], int depth);
void play_negamax_alpha_beta_depth_limited(State *state, int evaluation[3], int depth);
void play_negamax_alpha_beta_ordered_depth_limited(State *state, int evaluation[3], int depth);
void play_negamax_alpha_beta_ordered_ending(State *state, int evaluation[3], int depth, int end);
void play_negamax_alpha_beta_ordered_ending_quiscent(State *state, int evaluation[3],  int depth, int len_extension); // LEGACY
void play_negamax_alpha_beta_ordered_ending_forward_pruning_basic(State *state, int evaluation[3], int depth, int end, int nu_moves_to_play);
void play_negamax_alpha_beta_ordered_ending_forward_pruning_probcut(State *state, int evaluation[3], int depth, int end, int nu_moves_to_play);
void play_negamax_alpha_beta_ordered_ending_forward_pruning_basic_singular_extension(State *state, int evaluation[3],  int depth, int end, int nu_moves_to_play, int len_extension);