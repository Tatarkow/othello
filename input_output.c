#include <stdio.h>

#include "definitions.h"
#include "game_helper.h"
#include "input_output.h"

const int PLAY_BLACK = 1;
const int PLAY_WHITE = 2;
const int END_PROGRAM = 3;

void ask_for_command(void) {
	for(int i = 0; i < 50; i++) printf("\n");
	printf("1 - play as Black\n");
	printf("2 - play as White\n");
	printf("3 - Quit\n\n");
	printf("Type in the action action number: ");
}

int get_command(void) {
	char command;
	ask_for_command();
	command = getchar();

	while (command < '1' || command > '3') {
		ask_for_command();

		while (command != '\n') command = getchar(); // reads additional text
		command = getchar();
	}

	return command - '0';
}

void print_board(State state) {
	printf("╔══╦══╦══╦══╦══╦══╦══╦══╦══╗\n");
	if (state.player == BLACK) printf("║⨂ ");
	else printf("║⬤ ");
	printf("║A ║B ║C ║D ║E ║F ║G ║H ║");
	char row[] = "╠══╬══╬══╬══╬══╬══╬══╬══╬══╣";

	for (int i=0; i<8; i++) {
		printf("\n%s\n║ %d║", row, i+1);
		for (int j=0; j<8; j++) {
			char stone = get_stone_on(state, i, j);
			if (stone == BLACK) printf("⨂ ║");
			else if (stone == WHITE) printf("⬤ ║");
			else printf("  ║");
		}
	}

	printf("\n╚══╩══╩══╩══╩══╩══╩══╩══╩══╝\n");
}

int is_input_valid(char a, char b, uint64_t legal_moves) {
	int row = b - '0' - 1;
	if (row < 0 || row > 7) return FALSE;

	int column = a - 'a';
	if (column < 0 || column > 7) {
		column = a - 'A';
		if (column < 0 || column > 7) return FALSE;
	}

	if ((legal_moves & (1ULL << (row*8 + column))) == 0ULL) return FALSE;

	return TRUE;
}

uint64_t get_input(uint64_t legal_moves) {
	printf("Type in the move: ");

	char a = getchar();
	char b = getchar();

	while (! is_input_valid(a, b, legal_moves)) {
		while (a != '\n') a = getchar(); // reads additional text

		printf("The move is not valid!\nType in a new one: ");
		
		a = getchar();
		b = getchar();
	}

	int row = b - '0' - 1;
	int column = a - 'a';
	if (column < 0 || column > 7) column = a - 'A';

	while (a != '\n') a = getchar(); // reads additional text
	return 1ULL << (row*8 + column);
}

void play_human(State *state) {
	uint64_t legal_moves = get_legal_moves(*state);

	if (legal_moves == 0ULL) skip_move(state);
	else {
		uint64_t move = get_input(legal_moves);
		make_move(state, move);
	}
}