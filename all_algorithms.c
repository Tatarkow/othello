#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "definitions.h"
#include "input_output.h"
#include "game_helper.h"
#include "evaluation.h"
#include "all_algorithms.h"

void play_random(State *state) {
	uint64_t legal_moves = get_legal_moves(*state);
	
	if (legal_moves == 0ULL) skip_move(state);
	else {
		int nu_legal_moves = 0;
		for (int i = 0; i < 64; i++)
			if (((1ULL << i) & legal_moves) != 0ULL) nu_legal_moves++;

		make_random_move(state, legal_moves, nu_legal_moves);
	}
}

// -----------------------------------------------------------------------------

int negamax_depth_limited(State state, int evaluation[3], int depth) {
	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	if (depth == 0) return evaluate(state, evaluation);

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_depth_limited(new_state, evaluation, depth);
	}

	for (int i = 0; i < 64; i++) {
		if (((1ULL << i) & legal_moves) != 0ULL) {
			State new_state = state;
			make_move(&new_state, 1ULL << i);
			int value = negamax_depth_limited(new_state, evaluation, depth-1);
			if (state.player == WHITE) value = -value;
			if (value > max) max = value;
		}
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_depth_limited(State *state, int evaluation[3], int depth) {
	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << i) & legal_moves) != 0ULL) {
				State new_state = *state;
				make_move(&new_state, 1ULL << i);
				int value = negamax_depth_limited(new_state, evaluation, depth);
				if (state->player == WHITE) value = -value;

				if (value > max) {
					best_moves = (1ULL << i);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << i);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}

// -----------------------------------------------------------------------------

int negamax_alpha_beta_depth_limited(State state, int evaluation[3], int depth,
									 int alpha, int beta) {
	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	if (depth == 0) return evaluate(state, evaluation);

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_alpha_beta_depth_limited(new_state, evaluation, depth,
												-beta, -alpha);
	}

	for (int i = 0; i < 64; i++) {
		if (((1ULL << i) & legal_moves) != 0ULL) {
			State new_state = state;
			make_move(&new_state, 1ULL << i);
			int value = negamax_alpha_beta_depth_limited(new_state, evaluation,
														 depth-1, -beta,
														 -alpha);
			if (state.player == WHITE) value = -value;
			if (value > max) max = value;
			if (max > beta) break;
			if (max > alpha) alpha = max;
		}
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_alpha_beta_depth_limited(State *state, int evaluation[3],
										   int depth) {
	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << i) & legal_moves) != 0ULL) {
				State new_state = *state;
				make_move(&new_state, 1ULL << i);
				int value = negamax_alpha_beta_depth_limited(new_state,
															 evaluation, depth,
												 			 -MAX, -max);
				if (state->player == WHITE) value = -value;
				//printf("%d\n", value);

				if (value > max) {
					best_moves = (1ULL << i);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << i);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}

// -----------------------------------------------------------------------------

int negamax_alpha_beta_ordered_depth_limited(State state, int evaluation[3], int depth,
									 int alpha, int beta) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	if (depth == 0) return evaluate(state, evaluation);

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_alpha_beta_ordered_depth_limited(new_state, evaluation, depth,
												-beta, -alpha);
	}

	for (int i = 0; i < 64; i++) {
		if (((1ULL << order[i]) & legal_moves) != 0ULL) {
			State new_state = state;
			make_move(&new_state, 1ULL << order[i]);
			int value = negamax_alpha_beta_ordered_depth_limited(new_state, evaluation,
														 depth-1, -beta,
														 -alpha);
			if (state.player == WHITE) value = -value;
			if (value > max) max = value;
			if (max > beta) break;
			if (max > alpha) alpha = max;
		}
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_alpha_beta_ordered_depth_limited(State *state, int evaluation[3],
										   int depth) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				State new_state = *state;
				make_move(&new_state, 1ULL << order[i]);
				int value = negamax_alpha_beta_ordered_depth_limited(new_state,
															 evaluation, depth,
												 			 -MAX, -max);
				if (state->player == WHITE) value = -value;
				//printf("%d\n", value);

				if (value > max) {
					best_moves = (1ULL << order[i]);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << order[i]);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}

// -----------------------------------------------------------------------------

int negamax_alpha_beta_ordered_ending(State state, int evaluation[3],
									 int alpha, int beta) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_alpha_beta_ordered_ending(new_state, evaluation, -beta, -alpha);
	}

	for (int i = 0; i < 64; i++) {
		if (((1ULL << order[i]) & legal_moves) != 0ULL) {
			State new_state = state;
			make_move(&new_state, 1ULL << order[i]);
			int value = negamax_alpha_beta_ordered_ending(new_state, evaluation,-beta,
														 -alpha);
			if (state.player == WHITE) value = -value;
			if (value > max) max = value;
			if (max > beta) break;
			if (max > alpha) alpha = max;
		}
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_alpha_beta_ordered_ending(State *state, int evaluation[3],
										   int depth, int end) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	// THIS IS THE DIFFERENCE
	int left = count_stones(~ (state->black | state->white));

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				State new_state = *state;
				make_move(&new_state, 1ULL << order[i]);
				
				int value;
				if (left > end) value = negamax_alpha_beta_ordered_depth_limited(new_state,
															 evaluation, depth,
												 			 -MAX, -max);
				else value = negamax_alpha_beta_ordered_ending(new_state,
															 evaluation,
												 			 -MAX, -max);
				if (state->player == WHITE) value = -value;
				//printf("%d\n", value);

				if (value > max) {
					best_moves = (1ULL << order[i]);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << order[i]);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}

// -----------------------------------------------------------------------------
// LEGACY
int negamax_alpha_beta_ordered_depth_limited_quiscent(State state, int evaluation[3], int depth, int alpha, int beta, int len_extension) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	
	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_alpha_beta_ordered_depth_limited_quiscent(new_state, evaluation, depth,
												-beta, -alpha, len_extension);
	}

	if (depth == 0) {
		//printf("B: %d %d\n", depth, len_extension);
		if (len_extension == 0) return evaluate(state, evaluation);
		if (count_stones(legal_moves) > 1 && (legal_moves & 0xC3C300000000C3C3ULL) == 0ULL) return evaluate(state, evaluation);
		
		//printf("C: %d %d\n", depth, len_extension);
		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				State new_state = state;
				make_move(&new_state, 1ULL << order[i]);
				int value = negamax_alpha_beta_ordered_depth_limited_quiscent(new_state, evaluation,
															 depth, -beta,
															 -alpha, len_extension-1);
				if (state.player == WHITE) value = -value;
				if (value > max) max = value;
				if (max > beta) break;
				if (max > alpha) alpha = max;
			}
		}

		if (state.player == WHITE) max = -max;
		return max;
	}

	for (int i = 0; i < 64; i++) {
		if (((1ULL << order[i]) & legal_moves) != 0ULL) {
			State new_state = state;
			make_move(&new_state, 1ULL << order[i]);
			int value = negamax_alpha_beta_ordered_depth_limited_quiscent(new_state, evaluation,
														 depth-1, -beta,
														 -alpha, len_extension);
			if (state.player == WHITE) value = -value;
			if (value > max) max = value;
			if (max > beta) break;
			if (max > alpha) alpha = max;
		}
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_alpha_beta_ordered_ending_quiscent(State *state, int evaluation[3],  int depth, int len_extension) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	// THIS IS THE DIFFERENCE
	int left = count_stones(~ (state->black | state->white));

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				State new_state = *state;
				make_move(&new_state, 1ULL << order[i]);
				
				int value;
				if (left > 10) value = negamax_alpha_beta_ordered_depth_limited_quiscent(new_state,
															 evaluation, depth,
												 			 -MAX, -max, len_extension);
				else value = negamax_alpha_beta_ordered_ending(new_state,
															 evaluation,
												 			 -MAX, -max);
				if (state->player == WHITE) value = -value;
				//printf("%d\n", value);

				if (value > max) {
					best_moves = (1ULL << order[i]);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << order[i]);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}

// -----------------------------------------------------------------------------

int negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic(State state, int evaluation[3], int depth,
									 int alpha, int beta, int nu_moves_to_play) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	if (depth == 0) return evaluate(state, evaluation);

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic(new_state, evaluation, depth,
												-beta, -alpha, nu_moves_to_play);
	}

	int nu_moves_left = nu_moves_to_play;
	for (int i = 0; i < 64; i++) {
		if (nu_moves_left == 0) break;
		if (((1ULL << order[i]) & legal_moves) != 0ULL) {
			State new_state = state;
			make_move(&new_state, 1ULL << order[i]);
			int value = negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic(new_state, evaluation,
														 depth-1, -beta,
														 -alpha, nu_moves_to_play);
			if (state.player == WHITE) value = -value;
			if (value > max) max = value;
			if (max > beta) break;
			if (max > alpha) alpha = max;

			nu_moves_left--;
		}
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_alpha_beta_ordered_ending_forward_pruning_basic(State *state, int evaluation[3],
										   int depth, int end, int nu_moves_to_play) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	// THIS IS THE DIFFERENCE
	int left = count_stones(~ (state->black | state->white));

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				State new_state = *state;
				make_move(&new_state, 1ULL << order[i]);
				
				int value;
				if (left > end) value = negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic(new_state,
															 evaluation, depth,
												 			 -MAX, -max, nu_moves_to_play);
				else value = negamax_alpha_beta_ordered_ending(new_state,
															 evaluation,
												 			 -MAX, -max);
				if (state->player == WHITE) value = -value;
				//printf("%d\n", value);

				if (value > max) {
					best_moves = (1ULL << order[i]);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << order[i]);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}

// -----------------------------------------------------------------------------

int negamax_alpha_beta_ordered_depth_limited_forward_pruning_probcut(State state, int evaluation[3], int depth,
									 int alpha, int beta, int nu_moves_to_play) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	if (depth == 0) return evaluate(state, evaluation);

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_alpha_beta_ordered_depth_limited_forward_pruning_probcut(new_state, evaluation, depth,
												-beta, -alpha, nu_moves_to_play);
	}

	int squares[8][8] = {{ 4,-3, 2, 2, 2, 2,-3, 4},
						 {-3,-4,-1,-1,-1,-1,-4,-3},
						 { 2,-1, 1, 0, 0, 1,-1, 2},
						 { 2,-1, 0, 1, 1, 0,-1, 2},
						 { 2,-1, 0, 1, 1, 0,-1, 2},
						 { 2,-1, 1, 0, 0, 1,-1, 2},
						 {-3,-4,-1,-1,-1,-1,-4,-3},
						 { 4,-3, 2, 2, 2, 2,-3, 4}};

	int total_eval_squares = 0;
	for (int i = 0; i < 64; i++) {
		if (((1ULL << i) & legal_moves) != 0ULL) {
			total_eval_squares += 5 + squares[i / 8][i % 8]; // to make it possitive
		}
	}

	for (int move = 0; move < nu_moves_to_play; move++) {
		//printf("T: %d %d\n", total_eval_squares, depth);
		if (total_eval_squares == 0) break;
		int random_move_index = rand() % total_eval_squares;

		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				if (random_move_index <= 0) {
					//printf("T: %d %d\n", order[i], depth);
					State new_state = state;
					make_move(&new_state, 1ULL << order[i]);
					int value = negamax_alpha_beta_ordered_depth_limited_forward_pruning_probcut(new_state, evaluation,
																 depth-1, -beta,
																 -alpha, nu_moves_to_play);
					if (state.player == WHITE) value = -value;
					if (value > max) max = value;
					if (max > alpha) alpha = max;

					total_eval_squares -= 5 + squares[order[i] / 8][order[i] % 8];
					legal_moves ^= 1ULL << order[i];
					break;
				}
				else random_move_index -= 5 + squares[order[i] / 8][order[i] % 8];
			}
		}

		if (max > beta) break;
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_alpha_beta_ordered_ending_forward_pruning_probcut(State *state, int evaluation[3],
										   int depth, int end, int nu_moves_to_play) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	// THIS IS THE DIFFERENCE
	int left = count_stones(~ (state->black | state->white));

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				//printf("A\n");
				State new_state = *state;
				make_move(&new_state, 1ULL << order[i]);
				
				int value;
				if (left > end) value = negamax_alpha_beta_ordered_depth_limited_forward_pruning_probcut(new_state,
															 evaluation, depth,
												 			 -MAX, -max, nu_moves_to_play);
				else value = negamax_alpha_beta_ordered_ending(new_state,
															 evaluation,
												 			 -MAX, -max);
				if (state->player == WHITE) value = -value;
				//printf("%d\n", value);

				if (value > max) {
					best_moves = (1ULL << order[i]);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << order[i]);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}

// -----------------------------------------------------------------------------
// NOT LEGACY!!!
int negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic_singular_extension(State state, int evaluation[3], int depth, int alpha, int beta, int nu_moves_to_play, int len_extension) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	if (is_state_terminal(state)) {
		int nu_black_stones = count_stones(state.black);
		int nu_white_stones = count_stones(state.white);
		
		if (nu_black_stones > nu_white_stones) return MAX;
		if (nu_black_stones < nu_white_stones) return -MAX;
		if (nu_black_stones == nu_white_stones) return 0;
	}

	uint64_t legal_moves = get_legal_moves(state);
	int max = -MAX;

	if (depth == 0) return evaluate(state, evaluation);

	if (legal_moves == 0ULL) {
		State new_state = state;
		skip_move(&new_state);
		return negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic_singular_extension(new_state, evaluation, depth,
												-beta, -alpha, nu_moves_to_play, len_extension);
	}

	int nu_moves_left = nu_moves_to_play;
	for (int i = 0; i < 64; i++) {
		if (nu_moves_left == 0) break;
		if (((1ULL << order[i]) & legal_moves) != 0ULL) {
			State new_state = state;
			make_move(&new_state, 1ULL << order[i]);

			int value;
			// This is for singular extension
			if (depth == 1 && nu_moves_to_play == nu_moves_left && len_extension > 0)
				value = negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic_singular_extension(new_state, evaluation,
														 depth, -beta,
														 -alpha, nu_moves_to_play, len_extension-1);
			else value = negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic_singular_extension(new_state, evaluation,
														 depth-1, -beta,
														 -alpha, nu_moves_to_play, len_extension);
			if (state.player == WHITE) value = -value;
			if (value > max) max = value;
			if (max > beta) break;
			if (max > alpha) alpha = max;

			nu_moves_left--;
		}
	}

	if (state.player == WHITE) max = -max;
	return max;
}

void play_negamax_alpha_beta_ordered_ending_forward_pruning_basic_singular_extension(State *state, int evaluation[3],  int depth, int end, int nu_moves_to_play, int len_extension) {
	int order[64] = {0, 7, 56, 63, 2, 3, 4, 5, 16, 23, 24, 31, 32, 39, 40, 47,
					 58, 59, 60, 61, 18, 21, 27, 28, 35, 36, 42, 45, 19, 20, 26,
					 29, 34, 37, 43, 44, 10, 11, 12, 13, 17, 22, 25, 30, 33, 38,
					 41, 46, 50, 51, 52, 53, 1, 6, 8, 15, 48, 55, 57, 62, 9, 14,
					 49, 54};

	uint64_t legal_moves = get_legal_moves(*state);
	uint64_t best_moves = 0ULL;
	int nu_best_moves = 0;
	int max = -MAX; // it different from INT_MIN

	// THIS IS THE DIFFERENCE
	int left = count_stones(~ (state->black | state->white));

	if (legal_moves == 0ULL) skip_move(state);
	else {
		for (int i = 0; i < 64; i++) {
			if (((1ULL << order[i]) & legal_moves) != 0ULL) {
				State new_state = *state;
				make_move(&new_state, 1ULL << order[i]);
				
				int value;
				if (left > end) value = negamax_alpha_beta_ordered_depth_limited_forward_pruning_basic_singular_extension(new_state,
															 evaluation, depth,
												 			 -MAX, -max, nu_moves_to_play, len_extension);
				else value = negamax_alpha_beta_ordered_ending(new_state,
															 evaluation,
												 			 -MAX, -max);
				if (state->player == WHITE) value = -value;
				//printf("%d\n", value);

				if (value > max) {
					best_moves = (1ULL << order[i]);
					nu_best_moves = 1;
					max = value;
				}
				else if (value == max) {
					best_moves |= (1ULL << order[i]);
					nu_best_moves++;
				}
			}
		}

		make_random_move(state, best_moves, nu_best_moves);
	}
}