#include <inttypes.h>

#ifndef STATE_H_
#define STATE_H_

// Define a state of the game
typedef struct State {
	uint64_t black; // stores where the black stones are (row-wise)
	uint64_t white; // stores where the white stones are (row-wise)
	char player; // stores information about whose turn it is
	char nu_not_played_plies; // stores information about previous plies
} State;

// Constants for distinguishing players and different stones on the board
const char EMPTY;
const char BLACK;
const char WHITE;

// Constats to make code a bit prettier
const char FALSE;
const char TRUE;

// Constant for maximal value used as infinity
const int MAX;

#endif