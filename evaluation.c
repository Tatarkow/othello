#include <inttypes.h>

#include "definitions.h"
#include "game_helper.h"
#include "evaluation.h"

// First evalution method (LEGACY)
int eval_squares(State state) {
	int value = 0;
	
	// Note that e. g. (1ULL << 2) is the square in the bottom third from right
	int squares[8][8] = {{ 4,-3, 2, 2, 2, 2,-3, 4},
						 {-3,-4,-1,-1,-1,-1,-4,-3},
						 { 2,-1, 1, 0, 0, 1,-1, 2},
						 { 2,-1, 0, 1, 1, 0,-1, 2},
						 { 2,-1, 0, 1, 1, 0,-1, 2},
						 { 2,-1, 1, 0, 0, 1,-1, 2},
						 {-3,-4,-1,-1,-1,-1,-4,-3},
						 { 4,-3, 2, 2, 2, 2,-3, 4}};

	// Count value of the state
	for (int i = 0; i < 64; i++) {
		if (((1ULL << i) & state.black) != 0ULL) value += squares[i / 8][i % 8];
		if (((1ULL << i) & state.white) != 0ULL) value -= squares[i / 8][i % 8];
	}

	return value;
}

// Second evalution method
int eval_corners(State state) {
	const uint64_t CORNERS = 0x8100000000000081ULL; // corner mask
	int black = count_stones(state.black & CORNERS); // # of corner black stones
	int white = count_stones(state.white & CORNERS); // # of croner white stones

	return black - white;
}

// Third evalution method
int eval_moves(State state) {
	uint64_t player = get_legal_moves(state); // player's legal moves

	next_player(&state);
	uint64_t opponent = get_legal_moves(state); // opponent's legal moves

	if (state.player == BLACK) // this means that it is White's turn actually
		return count_stones(opponent) - count_stones(player);
	else
		return count_stones(player) - count_stones(opponent);
}

// Forth evalution method
int eval_frontier(State state) {
	uint64_t black = 0ULL; // black stones will be added here
	uint64_t white = 0ULL; // white stones will be added here
	uint64_t empty_squares = ~ (state.black | state.white);

	for (int direction = 0; direction < 8; direction++) {
		// Shift empty squares in every direction and try if there is an
		// intersection with either black or white stones
		uint64_t shifted_empty_squares = shift_stones(empty_squares, direction);
		black |= shifted_empty_squares & state.black;
		white |= shifted_empty_squares & state.white;
	}

	return count_stones(black) - count_stones(white);
}

// Combined evaluation function
int evaluate(State state, int weight[3]) {
	int corners = eval_corners(state);
	int moves = eval_moves(state);
	int frontier = eval_frontier(state);

	return weight[0]*corners + weight[1]*moves + weight[2]*frontier;
}