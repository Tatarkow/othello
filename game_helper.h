#ifndef GAME_HELPER_H_
#define GAME_HELPER_H_

void place_stone(State *state, uint64_t new_stone);
char get_stone_on(State state, int row, int column);
void reset_state(State *state);
uint64_t shift_stones(uint64_t stones, int direction);
uint64_t get_players_stones(State state);
uint64_t get_opponents_stones(State state);
uint64_t get_legal_moves(State state);
int is_state_terminal(State state);
void next_player(State *state);
void update_stones(State *state, uint64_t new_stones);
void skip_move(State *state);
void make_move(State *state, uint64_t move);
void make_random_move(State *state, uint64_t moves, int nu_moves);
int count_stones(uint64_t stones);
void play_game(int user);

#endif