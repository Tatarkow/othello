#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

#include "definitions.h"
#include "all_algorithms.h"
#include "game_helper.h"

void place_stone(State *state, uint64_t new_stone) {
	if (state->player == BLACK) state->black |= new_stone;
	else state->white |= new_stone;
}

char get_stone_on(State state, int row, int column) {
	uint64_t position = 1ULL << (8 * row + column);
	if ((state.black & position) != 0ULL) return BLACK;
	if ((state.white & position) != 0ULL) return WHITE;
	return EMPTY;
}

void reset_state(State *state) {
	// Set every square as empty
	state->black = 0ULL;
	state->white = 0ULL;

	// Put 4 stones on the board (in the initial position of the game)
	state->player = WHITE;
	place_stone(state, 0x0000001000000000ULL);
	place_stone(state, 0x0000000008000000ULL);

	state->player = BLACK;
	place_stone(state, 0x0000000800000000ULL);
	place_stone(state, 0x0000000010000000ULL);

	state->nu_not_played_plies = 0;
}

uint64_t shift_stones(uint64_t stones, int direction) {
	const uint64_t RIGHT_MASK = 0x7F7F7F7F7F7F7F7FULL;
	const uint64_t UP_RIGHT_MASK = 0x7F7F7F7F7F7F7F00ULL;
	const uint64_t UP_MASK = 0xFFFFFFFFFFFFFFFFULL;
	const uint64_t UP_LEFT_MASK = 0xFEFEFEFEFEFEFE00ULL;
	const uint64_t LEFT_MASK = 0xFEFEFEFEFEFEFEFEULL;
	const uint64_t DOWN_LEFT_MASK = 0x00FEFEFEFEFEFEFEULL;
	const uint64_t DOWN_MASK = 0xFFFFFFFFFFFFFFFFULL;
	const uint64_t DOWN_RIGHT_MASK = 0x007F7F7F7F7F7F7FULL;

	const uint64_t RIGHT = 1ULL;
	const uint64_t UP_RIGHT = 7ULL;
	const uint64_t UP = 8ULL;
	const uint64_t UP_LEFT = 9ULL;
	const uint64_t LEFT = 1ULL;
	const uint64_t DOWN_LEFT = 7ULL;
	const uint64_t DOWN = 8ULL;
	const uint64_t DOWN_RIGHT = 9ULL;

	switch(direction) {
		case 0: return RIGHT_MASK & (stones >> RIGHT);
		case 1: return UP_RIGHT_MASK & (stones << UP_RIGHT);
		case 2: return UP_MASK & (stones << UP);
		case 3: return UP_LEFT_MASK & (stones << UP_LEFT);
		case 4: return LEFT_MASK & (stones << LEFT);
		case 5: return DOWN_LEFT_MASK & (stones >> DOWN_LEFT);
		case 6: return DOWN_MASK & (stones >> DOWN);
		case 7: return DOWN_RIGHT_MASK & (stones >> DOWN_RIGHT);
	}
}

uint64_t get_players_stones(State state) {
	if (state.player == BLACK) return state.black;
	else return state.white;
}

uint64_t get_opponents_stones(State state) {
	if (state.player == BLACK) return state.white;
	else return state.black;
}

uint64_t get_legal_moves(State state) {
	uint64_t legal_moves = 0ULL;
	uint64_t empty_squares = ~ (state.black | state.white);
	uint64_t players_stones = get_players_stones(state);
	uint64_t opponents_stones = get_opponents_stones(state);
	
	for (int direction = 0; direction < 8; direction++) {
		uint64_t shifted_stones = shift_stones(players_stones, direction);
		uint64_t new_moves = shifted_stones & opponents_stones;	
		
		for (int i = 0; i < 5; i++) {
			shifted_stones = shift_stones(new_moves, direction);
			new_moves |= shifted_stones & opponents_stones;
		}
		
		legal_moves |= shift_stones(new_moves, direction) & empty_squares;
	}

	return legal_moves;
}

int is_state_terminal(State state) {
	if (state.nu_not_played_plies == 2) return TRUE;
	if (~ (state.black | state.white) == 0ULL) return TRUE;
	return FALSE;
}

void next_player(State *state) {
	if (state->player == BLACK) state->player = WHITE;
	else state->player = BLACK;
}

void update_stones(State *state, uint64_t new_stones) {
	if (state->player == BLACK) {
		state->black |= new_stones;
		state->white ^= new_stones & state->white;
	}
	else {
		state->white |= new_stones;
		state->black ^= new_stones & state->black;
	}
}

void skip_move(State *state) {
	state->nu_not_played_plies++;
	next_player(state);
}

void make_move(State *state, uint64_t move) {
	uint64_t new_stones = move;
	uint64_t players_stones = get_players_stones(*state);
	uint64_t opponents_stones = get_opponents_stones(*state);

	for (int direction = 0; direction < 8; direction++) {
		uint64_t all_shifted_stones = shift_stones(move, direction);
		uint64_t new_shifted_stone = all_shifted_stones;
		
		for (int i = 0; i < 7; i++) {
			if ((new_shifted_stone & players_stones) != 0ULL) {
				new_stones |= all_shifted_stones;
				break;
			}
			if ((new_shifted_stone & opponents_stones) == 0ULL)
				break;

			new_shifted_stone = shift_stones(new_shifted_stone, direction);
			all_shifted_stones |= new_shifted_stone;
		}
	}

	state->nu_not_played_plies = 0;
	update_stones(state, new_stones);
	next_player(state);
}

void make_random_move(State *state, uint64_t moves, int nu_moves) {
	int index = 1 + rand() % nu_moves;
		for (int i = 0; i < 64; i++) {
			if (((1ULL << i) & moves) != 0ULL) index--;
			if (index == 0) {
				make_move(state, (1ULL << i));
				break;
			}
		}
}

int count_stones(uint64_t stones) {
	int nu_stones = 0;
	for (int i = 0; i < 64; i++) {
		if (((1ULL << i) & stones) != 0ULL) nu_stones++;
	}
	return nu_stones;
}

void play_game(int user) {
	State state; // creates a new state
	reset_state(&state);
	
	int evaluation[3] = {16, 2, -1};
	int depth = 6;
	int end_search = 10;
	int forward_pruning = 8;
	int singular_extension = 0;

	while (! is_state_terminal(state)) {
		for(int i = 0; i < 50; i++) printf("\n");
		print_board(state);
		char a;
		while (a != '\n') a = getchar(); // reads additional characters

		if (user == BLACK) {
			if (state.player == BLACK)
				play_human(&state);
			else
				play_negamax_alpha_beta_ordered_ending_forward_pruning_basic_singular_extension(
					&state, evaluation, depth, end_search, forward_pruning, singular_extension);
		}
		else {
			if (state.player == BLACK)
				play_negamax_alpha_beta_ordered_ending_forward_pruning_basic_singular_extension(
					&state, evaluation, depth, end_search, forward_pruning, singular_extension);
			else
				play_human(&state);
		}
	}

	// End of the game
	for(int i = 0; i < 50; i++) printf("\n");
	print_board(state);

	int nu_black_stones = count_stones(state.black);
	int nu_white_stones = count_stones(state.white);
	
	if (nu_black_stones > nu_white_stones) {
		if (user == BLACK) printf("\nYOU WON! (score: %d : %d)\n", nu_black_stones, nu_white_stones);
		else printf("\nYOU LOST! (score: %d : %d)\n", nu_white_stones, nu_black_stones);
	}
	else if (nu_black_stones < nu_white_stones) {
		if (user == BLACK) printf("\nYOU LOST! (score: %d : %d)\n", nu_black_stones, nu_white_stones);
		else printf("\nYOU WON! (score: %d : %d)\n", nu_white_stones, nu_black_stones);
	}
	else printf("\nDRAW! (score: %d : %d)\n", nu_black_stones, nu_white_stones);
	
}